import { Rule } from "../rule";
import {  TagReadyEvent } from "../event";

export default class NoDupAttr extends Rule {
	// TODO: documentation

	public setup(): void {

		this.on("tag:ready", this.isRelevant, (event: TagReadyEvent) => {
			const { target } = event;
			if (target.hasAttribute("id") && event.target.hasAttribute("name")) {
				if (target.getAttributeValue("id") === target.getAttributeValue("name"))
					this.report(event.target, `Attributes id and name should be equal on map tag`, target.location);
			}
		});
	}

	protected isRelevant(this: void, event: TagReadyEvent): boolean {
		return event.target.is("map");
	}
}
