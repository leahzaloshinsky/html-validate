import "../jest";
import HtmlValidate from "../htmlvalidate";

describe("rule map-id-name", () => {
	let htmlvalidate: HtmlValidate;

	beforeAll(() => {
		htmlvalidate = new HtmlValidate({
			root: true,
			rules: { "map-id-name": "error" },
		});
	});

	it("should not report when map and id are not equal", () => {
		expect.assertions(1);
		const report = htmlvalidate.validateString('<map id="foo" name="bar"></map>');
		expect(report).toBeValid();
	});

	it("should report when name and id are equal", () => {
		expect.assertions(2);
		const report = htmlvalidate.validateString('<map id="foo" name="foo"></map>');
		expect(report).toBeInvalid();
		expect(report).toHaveError("map-id-name", 'Attributes id and name should be equal on map tag');
	});

	it("should report when name and id are equal without case insensitive", () => {
		expect.assertions(2);
		const report = htmlvalidate.validateString('<map Id="foo" name="Foo"></map>');
		expect(report).toBeInvalid();
		expect(report).toHaveError("map-id-name", 'Attributes id and name should be equal on map tag');
	});

	// TODO:
	// it("smoketest", () => {
	// 	expect.assertions(1);
	// 	const report = htmlvalidate.validateFile("test-files/rules/map-id-name.html");
	// 	expect(report.results).toMatchSnapshot();
	// });

	// it("should contain documentation", () => {
	// 	expect.assertions(1);
	// 	expect(htmlvalidate.getRuleDocumentation("map-id-name")).toMatchSnapshot();
	// });
});
